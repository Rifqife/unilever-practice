﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UnileverPractice.Layouts
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Homepage : ContentPage
	{
		public Homepage ()
		{
			InitializeComponent ();

            btnNextPage.Clicked += OnBtnNextPageClicked;
		}

        void OnBtnNextPageClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Question());
        }
	}
}