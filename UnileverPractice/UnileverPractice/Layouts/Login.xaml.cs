﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using PerpetualEngine.Storage;

namespace UnileverPractice.Layouts
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Login : ContentPage
	{
        String email, password, jobTitle;

		public Login ()
		{
			InitializeComponent();

#if __ANDROID__
            SimpleStorage.SetContext(Android.App.Application.Context);
#endif

            btnLogin.Clicked += OnBtnLoginClicked;
        }

        public void OnBtnLoginClicked(object sender, EventArgs e)
        {
            email = txtEmail.Text;
            password = txtPassword.Text;
            jobTitle = txtJobTitle.Text;

            var loginData = SimpleStorage.EditGroup("loginData");
            loginData.Put("email", email);
            loginData.Put("password", password);
            loginData.Put("jobTitle", jobTitle);

            if (email == "yurika nazmilia pulungan" && password == "rifqi")
            {
                Navigation.PushAsync(new Homepage());
            }
            else
            {
                Console.WriteLine("Salah memasukkan Email atau Password");
            }
        }
	}
}